;;;; VOPs and other machine-specific support routines for call-out to C

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

(defconstant +number-stack-allocation-granularity+ n-word-bytes)

(defconstant +max-register-args+ 4)

(defun my-make-wired-tn (prim-type-name sc-name offset)
  (make-wired-tn (primitive-type-or-lose prim-type-name )
                 (sc-number-or-lose sc-name )
                 offset))

(defstruct arg-state
  (next-double-register 0)
  (next-single-register 0)
  (num-register-args 0)
  (stack-frame-size 0))

(defstruct (result-state (:copier nil))
  (num-results 0))

(defun result-reg-offset (slot)
  (ecase slot
    (0 nargs-offset)
    (1 nl3-offset)))

(defun register-args-offset (index)
  (elt '(#.ocfp-offset #.nargs-offset #.nl2-offset #.nl3-offset)
       index))

(defun int-arg (state prim-type reg-sc stack-sc)
  (let ((reg-args (arg-state-num-register-args state)))
    (cond ((< reg-args +max-register-args+)
           (setf (arg-state-num-register-args state) (1+ reg-args))
           (my-make-wired-tn prim-type reg-sc (register-args-offset reg-args)))
          (t
           (let ((frame-size (arg-state-stack-frame-size state)))
             (setf (arg-state-stack-frame-size state) (1+ frame-size))
             (my-make-wired-tn prim-type stack-sc frame-size))))))

(define-alien-type-method (integer :arg-tn) (type state)
  (if (alien-integer-type-signed type)
      (int-arg state 'signed-byte-32 'signed-reg 'signed-stack)
      (int-arg state 'unsigned-byte-32 'unsigned-reg 'unsigned-stack)))

(define-alien-type-method (system-area-pointer :arg-tn) (type state)
  (declare (ignore type))
  (int-arg state 'system-area-pointer 'sap-reg 'sap-stack))

#!+arm-softfp
(define-alien-type-method (single-float :arg-tn) (type state)
  (declare (ignore type))
  (int-arg state 'single-float 'unsigned-reg 'single-stack))

#!-arm-softfp
(define-alien-type-method (single-float :arg-tn) (type state)
  (declare (ignore type))
  (let ((register (arg-state-next-single-register state)))
    (when (> register 15)
      (error "Don't know how to handle alien single floats on stack."))
    (prog1
        (my-make-wired-tn 'single-float 'single-reg register)
      (setf (arg-state-next-single-register state)
            (max (1+ register)
                 (* 2 (arg-state-next-double-register state))))
      (setf (arg-state-next-double-register state)
            (+ (/ (arg-state-next-single-register state) 2)
               (if (evenp (arg-state-next-single-register state)) 0 1))))))

#!+arm-softfp
(define-alien-type-method (double-float :arg-tn) (type state)
  (declare (ignore type))
  (let* ((register (arg-state-num-register-args state))
         ;; The registers used are aligned, only r0-r1 and r2-r3 pairs
         ;; can be used.
         (register (+ register (logand register 1))))
    (cond ((> (+ register 2) +max-register-args+)
           (setf (arg-state-num-register-args state) +max-register-args+)
           (let ((frame-size (arg-state-stack-frame-size state)))
             (setf (arg-state-stack-frame-size state) (+ frame-size 2))
             (my-make-wired-tn 'double-float 'double-stack frame-size)))
          (t
           (setf (arg-state-num-register-args state) (+ register 2))
           (list
            (my-make-wired-tn 'unsigned-byte-32 'unsigned-reg
                              (register-args-offset register))
            (my-make-wired-tn 'unsigned-byte-32 'unsigned-reg
                              (register-args-offset (1+ register)))
            'move-double-to-int-args)))))

#!-arm-softfp
(define-alien-type-method (double-float :arg-tn) (type state)
  (declare (ignore type))
  (let ((register (arg-state-next-double-register state)))
    (when (> register 7)
      (error "Don't know how to handle alien double floats on stack."))
    (prog1
        (my-make-wired-tn 'double-float 'double-reg (* register 2))
      (incf (arg-state-next-double-register state))
      (when (evenp (arg-state-next-single-register state))
        (incf (arg-state-next-single-register state) 2)))))

(define-alien-type-method (integer :result-tn) (type state)
  (let ((num-results (result-state-num-results state)))
    (setf (result-state-num-results state) (1+ num-results))
    (multiple-value-bind (ptype reg-sc)
        (if (alien-integer-type-signed type)
            (values 'signed-byte-32 'signed-reg)
            (values 'unsigned-byte-32 'unsigned-reg))
      (my-make-wired-tn ptype reg-sc
                        (result-reg-offset num-results)))))

(define-alien-type-method (system-area-pointer :result-tn) (type state)
  (declare (ignore type state))
  (my-make-wired-tn 'system-area-pointer 'sap-reg nargs-offset))

#!+arm-softfp
(define-alien-type-method (single-float :result-tn) (type state)
  (declare (ignore type state))
  (my-make-wired-tn 'single-float 'unsigned-reg nargs-offset))

#!-arm-softfp
(define-alien-type-method (single-float :result-tn) (type state)
  (declare (ignore type state))
  (my-make-wired-tn 'single-float 'single-reg 0))

#!+arm-softfp
(define-alien-type-method (double-float :result-tn) (type state)
  (declare (ignore type state))
  (list (my-make-wired-tn 'unsigned-byte-32 'unsigned-reg nargs-offset)
        (my-make-wired-tn 'unsigned-byte-32 'unsigned-reg nl3-offset)
        'move-int-args-to-double))

#!-arm-softfp
(define-alien-type-method (double-float :result-tn) (type state)
  (declare (ignore type state))
  (my-make-wired-tn 'double-float 'double-reg 0))

(define-alien-type-method (values :result-tn) (type state)
  (let ((values (alien-values-type-values type)))
    (when (> (length values) 2)
      (error "Too many result values from c-call."))
    (mapcar (lambda (type)
              (invoke-alien-type-method :result-tn type state))
            values)))

(defun make-call-out-tns (type)
  (let ((arg-state (make-arg-state)))
    (collect ((arg-tns))
      (dolist (arg-type (alien-fun-type-arg-types type))
        (arg-tns (invoke-alien-type-method :arg-tn arg-type arg-state)))
      (values (make-normal-tn *fixnum-primitive-type*)
              (* (arg-state-stack-frame-size arg-state) n-word-bytes)
              (arg-tns)
              (invoke-alien-type-method :result-tn
                                        (alien-fun-type-result-type type)
                                        (make-result-state))))))

(define-vop (foreign-symbol-sap)
  (:translate foreign-symbol-sap)
  (:policy :fast-safe)
  (:args)
  (:arg-types (:constant simple-string))
  (:info foreign-symbol)
  (:temporary (:sc interior-reg) lip)
  (:results (res :scs (sap-reg)))
  (:result-types system-area-pointer)
  (:generator 2
    (let ((fixup-label (gen-label)))
      (inst load-from-label res lip fixup-label)
      (assemble (*elsewhere*)
        (emit-label fixup-label)
        (inst word (make-fixup foreign-symbol :foreign))))))

#!+linkage-table
(define-vop (foreign-symbol-dataref-sap)
  (:translate foreign-symbol-dataref-sap)
  (:policy :fast-safe)
  (:args)
  (:arg-types (:constant simple-string))
  (:info foreign-symbol)
  (:temporary (:sc interior-reg) lip)
  (:results (res :scs (sap-reg)))
  (:result-types system-area-pointer)
  (:generator 2
    (let ((fixup-label (gen-label)))
      (inst load-from-label res lip fixup-label)
      (inst ldr res (@ res))
      (assemble (*elsewhere*)
        (emit-label fixup-label)
        (inst word (make-fixup foreign-symbol :foreign-dataref))))))

(define-vop (call-out)
  (:args (function :scs (sap-reg sap-stack))
         (args :more t))
  (:results (results :more t))
  (:ignore args results)
  (:save-p t)
  (:temporary (:sc any-reg :offset r8-offset
                   :from (:argument 0) :to (:result 0)) cfunc)
  (:temporary (:sc control-stack :offset nfp-save-offset) nfp-save)
  (:temporary (:sc any-reg) temp)
  (:temporary (:sc interior-reg) lip)
  (:vop-var vop)
  (:generator 0
    (let ((call-into-c-fixup (gen-label))
          (cur-nfp (current-nfp-tn vop)))
      (assemble (*elsewhere*)
        (emit-label call-into-c-fixup)
        (inst word (make-fixup "call_into_c" :foreign)))
      (when cur-nfp
        (store-stack-tn nfp-save cur-nfp))
      (inst load-from-label temp lip call-into-c-fixup)
      (sc-case function
        (sap-reg (move cfunc function))
        (sap-stack (loadw cfunc cur-nfp (tn-offset function))))
      (inst blx temp)
      (when cur-nfp
        (load-stack-tn cur-nfp nfp-save)))))

(define-vop (alloc-number-stack-space)
  (:info amount)
  (:result-types system-area-pointer)
  (:results (result :scs (sap-reg any-reg)))
  (:generator 0
    (unless (zerop amount)
      (let ((delta (logandc2 (+ amount (1- +number-stack-allocation-granularity+))
                             (1- +number-stack-allocation-granularity+))))
        (composite-immediate-instruction sub nsp-tn nsp-tn delta)
        (move result nsp-tn)))))

(define-vop (dealloc-number-stack-space)
  (:info amount)
  (:policy :fast-safe)
  (:generator 0
    (unless (zerop amount)
      (let ((delta (logandc2 (+ amount (1- +number-stack-allocation-granularity+))
                             (1- +number-stack-allocation-granularity+))))
        (composite-immediate-instruction add nsp-tn nsp-tn delta)))))
;;;

#!+arm-softfp
(define-vop (move-double-to-int-args)
  (:args (double :scs (double-reg)))
  (:results (lo-bits :scs (unsigned-reg))
            (hi-bits :scs (unsigned-reg)))
  (:arg-types double-float)
  (:result-types unsigned-num unsigned-num)
  (:policy :fast-safe)
  (:generator 2
    (inst fmrdl lo-bits double)
    (inst fmrdh hi-bits double)))

#!+arm-softfp
(define-vop (move-int-args-to-double)
  (:args (lo-bits :scs (unsigned-reg))
         (hi-bits :scs (unsigned-reg)))
  (:results (double :scs (double-reg)))
  (:arg-types unsigned-num unsigned-num)
  (:result-types double-float)
  (:policy :fast-safe)
  (:generator 2
    (inst fmdlr double lo-bits)
    (inst fmdhr double hi-bits)))

;;; long-long support
(deftransform %alien-funcall ((function type &rest args) * * :node node)
  (aver (sb!c::constant-lvar-p type))
  (let* ((type (sb!c::lvar-value type))
         (env (sb!c::node-lexenv node))
         (arg-types (alien-fun-type-arg-types type))
         (result-type (alien-fun-type-result-type type)))
    (aver (= (length arg-types) (length args)))
    (if (or (some (lambda (type)
                    (and (alien-integer-type-p type)
                         (> (sb!alien::alien-integer-type-bits type) 32)))
                  arg-types)
            (and (alien-integer-type-p result-type)
                 (> (sb!alien::alien-integer-type-bits result-type) 32)))
        (collect ((new-args) (lambda-vars) (new-arg-types))
                 (loop with i = 0
                       for type in arg-types
                       for arg = (gensym)
                       do
                       (lambda-vars arg)
                       (cond ((and (alien-integer-type-p type)
                                   (> (sb!alien::alien-integer-type-bits type) 32))
                              (when (oddp i)
                                ;; long-long is only passed in pairs of r0-r1 and r2-r3,
                                ;; and the stack is double-word aligned
                                (incf i)
                                (new-args 0)
                                (new-arg-types (parse-alien-type '(signed 8) env)))
                              (incf i 2)
                              (new-args `(logand ,arg #xffffffff))
                              (new-args `(ash ,arg -32))
                              (new-arg-types (parse-alien-type '(unsigned 32) env))
                              (if (alien-integer-type-signed type)
                                  (new-arg-types (parse-alien-type '(signed 32) env))
                                  (new-arg-types (parse-alien-type '(unsigned 32) env))))
                             (t
                              (incf i (cond ((or (alien-double-float-type-p type)
                                                 #!-arm-softfp (alien-single-float-type-p type))
                                             #!+arm-softfp 2
                                             #!-arm-softfp 0)
                                            (t
                                             1)))
                              (new-args arg)
                              (new-arg-types type))))
                 (cond ((and (alien-integer-type-p result-type)
                             (> (sb!alien::alien-integer-type-bits result-type) 32))
                        (let ((new-result-type
                                (let ((sb!alien::*values-type-okay* t))
                                  (parse-alien-type
                                   (if (alien-integer-type-signed result-type)
                                       '(values (unsigned 32) (signed 32))
                                       '(values (unsigned 32) (unsigned 32)))
                                   env))))
                          `(lambda (function type ,@(lambda-vars))
                             (declare (ignore type))
                             (multiple-value-bind (low high)
                                 (%alien-funcall function
                                                 ',(make-alien-fun-type
                                                    :arg-types (new-arg-types)
                                                    :result-type new-result-type)
                                                 ,@(new-args))
                               (logior low (ash high 32))))))
                       (t
                        `(lambda (function type ,@(lambda-vars))
                           (declare (ignore type))
                           (%alien-funcall function
                                           ',(make-alien-fun-type
                                              :arg-types (new-arg-types)
                                              :result-type result-type)
                                           ,@(new-args))))))
        (sb!c::give-up-ir1-transform))))
